# Attack Tree Generator

A script to generate attack trees from text files.

Example attack tree in plain text, a collection of attacks can be found in `src/`:

	Become Root : OR
		No-auth : SAND
			Gain user privileges : SAND
				ftp
				rsh
			Local Buffer Overflow 
		Auth : AND
			ssh 
			rsa

Example attack tree as an image, check `dist/` for more examples:

![example attack tree](example.png)

# Install

The easiest way to install, is to use conda. As described on ete3's [website](http://etetoolkit.org/download/).

 	python3 -m venv venv         # Create a virtual environment
 	. venv/bin/activate          # Activate it
 	pip install ete3 six PyQt5   # Install the requirements

**Note** - This does not work on wayland. PyQT is needed by ete and currently throws up an error on wayland. (*Might be fixed as of Nov-2019*)

# Usage

	make                    # Builds all
	make dist/example.pdf   # Build example.ctrees 

To force a build when the .ctrees file has not been modified, update the timestamp `touch src/*.ctrees`.

# Attack Trees License

The atack tress, both src and pdfs are licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-nc-sa/4.0/). 

# Code License 

Copyright (c) 2019 Peter Maynard

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
