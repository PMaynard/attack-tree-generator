#!/bin/python

from ete3 import Tree, TreeStyle, NodeStyle, TextFace, faces
import csv, argparse

join_color = "Orange"
sand_color = "#BC3EE6"
and_color = "#68E63E"
or_color = "#3EBCE6"

class Node:
	def __init__(self, id, parent, children, operator, name, indent):
		self.id = id
		self.parent = parent
		self.children = []
		self.operator = operator
		self.name = name.title()
		self.indent = indent

	# def nodeDot(self):
	# 	shape = "ellipse"
		
	# 	if self.operator == "JOIN":
	# 		shape = "component"

	# 	return "\"{}\" [{}, {}, label=\"{}\" weight=\"{}\"];\r\n".format(self.id, "fontname=sans", "shape="+shape, self.name, self.id)

	# def edgeDot(self):
	# 	arrowHead = "none" # Default OR
	# 	colour = "#3EBCE6" # Default OR

	# 	if self.operator == "SAND":
	# 		arrowHead = "dot"
	# 		colour = "#BC3EE6" # Purpleish BC3EE6 C390D4
	# 	elif self.operator == "AND":
	# 		arrowHead = "normal"
	# 		colour = "#68E63E" # Greenish 68E63E 90C3D4
	# 	elif self.operator == "OR":
	# 		arrowHead = "none"
	# 		colour = "#3EBCE6" # Blueish 3EBCE6 A1D490
		
	# 	# print self.name, "is child of", self.parent.name, self.parent.indent

	# 	label = "label=\"{}.{}\"".format(self.parent.id+1, self.getPostion() )

	# 	edge = "\"{}\" -> \"{}\" [{}, {}, {}];\r\n".format(
	# 		self.id, 
	# 		self.parent.id, 
	# 		"arrowhead="+arrowHead, 
	# 		"color=\""+colour+"\"", 
	# 		label
	# 	)
	# 	return edge

	# def getPostion(self):
	# 	if self.parent:
	# 		for i in range(len(self.parent.children)):
	# 			if self.id == self.parent.children[i].id:
	# 				# print "Name:\t%s\tID:\t%s\tIndent:%s\ti:%s\t[%s]\n" % (self.name, self.id, self.indent, i, i+self.indent)
	# 				return i+1
	# 	return -1

def main(read, write):
	nodes = []

	# Read tab indented file
	with open(read, 'rt') as csvfile:
		reader = csv.reader(csvfile, dialect=csv.excel_tab)
		id = 0
		for row in reader:
			# Count the number of indents.
			indent = 0
			for i in row:
				if i == '':
					indent = indent+1

			# Extract the name and operator.
			try: 
				name = row[-1].split(":")[0].strip()
			except: 
				name = ""
			try:
				operator = row[-1].split(":")[1].strip()
			except:
				operator = "OR"

			# Ignore comments
			if name and name[0] != '#':
				tmp_node = Node(id, None, None, operator, name, indent)
				nodes.append(tmp_node)
				id += 1
			else: 
				if not name == "": 
					pass
					#print "[-] Skipping comment -", name[1:]

	# Work out whoes who.
	for i in range(len(nodes)):
		# Skip root.
		if i == 0:
			continue

		# Look backwards and find the parent node (indent-1) 
		for k in range(i):
			if (nodes[i].indent -1) == nodes[k].indent:
				nodes[i].parent = nodes[k]

	# In the correct order add the children.
	for n in nodes:
		try:
			n.parent.children.append(n)
		except:
			pass

	t = Tree()
	ts = TreeStyle()
	ts.show_scale = False
	ts.force_topology = True
	ts.extra_branch_line_color = "black"
	ts.extra_branch_line_type = 0

	ns_join = NodeStyle()
	ns_join['hz_line_width'] = 2
	ns_join['vt_line_width'] = 2
	ns_join['shape'] = "square"
	ns_join["size"] = 7
	ns_join["fgcolor"] = "black"

	ns_sand = NodeStyle()
	ns_sand['hz_line_width'] = 2
	ns_sand['vt_line_width'] = 2
	ns_sand['shape'] = "circle"
	ns_sand["size"] = 7
	ns_sand["fgcolor"] = "black"

	ns_and = NodeStyle()
	ns_and['hz_line_width'] = 2
	ns_and['vt_line_width'] = 2
	ns_and["size"] = 0
	ns_and["fgcolor"] = "white"

	ns_or = NodeStyle()
	ns_or['hz_line_width'] = 2
	ns_or['vt_line_width'] = 2
	ns_or['vt_line_type'] = 2
	ns_or["size"] = 0
	ns_or["fgcolor"] = "white"

	for i in nodes:
		subID = 0
		if i.parent:
			for x in range(len(i.parent.children)):
				thisOne = i.parent.children[x]
				if thisOne.id == i.id:
					subID = x+1
		label = "%s " % (i.name)

		# Check Root
		if i.id == 0:
			node = t.add_child(name=i.name)
			node.dist = 1
			node.add_features(operator=i.operator)
			node.add_features(id=i.id)
			node.add_face(TextFace(label), column=0, position="branch-top")
			# Create the root node style.
			if i.operator == "AND":
				node.set_style(ns_and)
			elif i.operator == "OR":
				node.set_style(ns_or)
			elif i.operator == "SAND":
				node.set_style(ns_sand)
			continue

		i.name = " " + i.name
		parent = t.search_nodes(id=i.parent.id)
		new_node = parent[0].add_child(name=i.name)

		# make the internal show up. [add id number to the steps.]
		dist_root = t.get_distance(new_node,t.get_tree_root(), topology_only=True)
		label = " (%d.%d.%d) %s" % (parent[0].dist, dist_root, subID, label)
		if i.children:
			new_node.add_face(TextFace(label), column=0, position="branch-top")
		else:
			new_node.name = label

		new_node.dist = subID
		new_node.add_features(operator=i.operator)
		new_node.add_features(id=i.id)


		if new_node.operator == "JOIN":
			new_node.set_style(ns_join)

		if new_node.operator == "SAND":
			new_node.set_style(ns_sand)

		if new_node.operator == "AND":
			new_node.set_style(ns_and)

		if new_node.operator == "OR":
			new_node.set_style(ns_or)

	# print t.write(format=1)
	# t.show(tree_style=ts)
	t.render(write, tree_style=ts)
	# print t.get_ascii(attributes=["name", "operator"], show_internal=True )
	# for i in t: 
	# 	print i.get_topology_id("Become root")

if __name__ == "__main__":
	parser = argparse.ArgumentParser(description='Tab indented lines to image')
	
	parser.add_argument('-f', '--file', dest='file', action='store', 
		help='Tab indented file to read.', required=True)

	parser.add_argument('-w', '--write', dest='output', action='store', 
		help='Output image accepts: .pdf, .svg, .png', required=True)

	args = parser.parse_args()
	main(args.file, args.output)
