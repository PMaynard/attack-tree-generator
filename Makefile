# https://gist.github.com/isaacs/62a2d1825d04437c6f08
# make // Builds all
# make dist/example.pdf // Build example.ctrees 
#
# To force a build: touch src/*.ctrees

# ../figures/3/%.pdf: src/%.ctrees
# 	 python convert-to-tree.py -f $< -w $@

all: $(patsubst src/%.ctrees,dist/%.pdf,$(shell echo src/*.ctrees))

dist/%.pdf: src/%.ctrees
	 python convert-to-tree.py -f $< -w $@

.PHONY: all
